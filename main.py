import os

import discord

from topette.plugins import OnlyPhotosPlugin, PresentationPlugin, ValentinPlugin
from topette.topette_base import TopetteBase

def_intents = discord.Intents.default()
def_intents.message_content = True
t = TopetteBase(intents=def_intents, db_url='sqlite:///db/topette.db')
t.register(OnlyPhotosPlugin())
t.register(PresentationPlugin())
t.register(ValentinPlugin())
t.run(os.environ.get('TOKEN'))
