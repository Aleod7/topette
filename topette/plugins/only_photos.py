import discord

from topette.abstract_plugins import ChannelWatcherPlugin


class OnlyPhotosPlugin(ChannelWatcherPlugin):
    """TopettePlugin that allows only messages with attachments (mainly for photos) in some channels. Any message
    without attachment will immediately be deleted
    """

    async def _validate(self, message: discord.Message) -> bool:
        return bool(message.attachments)
