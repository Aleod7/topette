import os
import random
from typing import Optional

from discord import Embed
from discord.abc import GuildChannel
from discord.ext.commands import Context
from sqlalchemy import Column, Integer, String, Boolean

from topette.topette_base import TopettePlugin, Base, check_roles


class ValentinPlugin(TopettePlugin):
    _channel: GuildChannel

    def __init__(self, channel_id: Optional[int] = None):
        self._channel_id = channel_id or int(os.environ.get('VALENTINPLUGIN_CHANNEL_ID'))

    async def on_ready(self):
        self._channel = await self._client.fetch_channel(self._channel_id)

    async def _cmd_valentin(self, ctx: Context, message: str):
        with self.session as session:
            val = ValentinModel(user_id=ctx.author.id, message=message)
            session.add(val)
            session.commit()

    @check_roles(['Fonda', 'Sous-Gérant/e', 'Modoo', 'Organisateur/trice', 'Animateur'])
    async def _cmd_release_valentin(self, ctx: Context):
        with self.session as session:
            vals = session.query(ValentinModel).where(ValentinModel.is_sent == False).all()
            for val in vals:
                red = random.randint(0xa0, 0xff)
                green = random.randint(0x50, 0x80)
                blue = green + random.randint(-0x20, 0x20)
                color = (red << 16) + (green << 8) + blue
                await self._channel.send(
                    embed=Embed(title="Déclaration anonyme", description=val.message, color=color))
                val.is_sent = True
            session.add_all(vals)
            session.commit()


class ValentinModel(Base):
    __tablename__ = 'valentin'

    id = Column(Integer, primary_key=True)
    user_id = Column(String(256))
    message = Column(String)
    is_sent = Column(Boolean, default=False)
