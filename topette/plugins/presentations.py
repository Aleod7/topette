import re

import discord

from topette.abstract_plugins import ChannelWatcherPlugin

EXAMPLE_PRES = """╔════════════ IDENTITE ═════════════════════╗
╠ Prénom : Topette
╠ Âge : 1 ans
╠ Sexe : Autre
╠ Activité : Bot
╚═══════════════════════════════════════╝
╔════════════ APPARENCE ═══════════════════╗
╠ Taille : 1.69m
╠ Poids : 42kg
╠ Couleur des yeux : Gris
╠ Couleur des cheveux : Roux 
╚═══════════════════════════════════════╝
╔════════════ PERSONNALITE ═════════════════╗
╠ Caractère : Strict, rigoureux
╠ Hobbies : Modérer le discord
╠ MP : Ouverts
╚═══════════════════════════════════════╝"""
REGEX_PRES = (
    r"^╔═+ IDENTITE ═+╗\s+"
    r"╠ Prénom : .*\s+"
    r"╠ Âge : .* ans\s+"
    r"╠ Sexe : (Homme|Femme|Autre)\s+"
    r"(╠ .* : .*\s+)*"
    r"╚═+╝\s+"
    r"(╔═+ \w+ ═+╗\s+"
    r"(╠ .* : .*\s+)*"
    r"╚═+╝\s+)*"
    r"╔═+ PERSONNALITE ═+╗\s+"
    r"(╠ .* : .*\s+)*"
    r"╠ MP : .*\s+"
    r"(╠ .* : .*\s+)*"
    r"╚═+╝\s*$"
)

assert re.match(REGEX_PRES, EXAMPLE_PRES), "The example pres does not match the regex"


class PresentationPlugin(ChannelWatcherPlugin):
    VALID_PRESENTATION = re.compile(REGEX_PRES, re.MULTILINE)

    async def _validate(self, message: discord.Message) -> bool:
        return bool(self.VALID_PRESENTATION.match(message.content))

    async def _on_invalid_message(self, message: discord.Message, history=False):
        if history:
            return
        embed = discord.Embed(title="Invalid presentation message")
        embed.add_field(name="Invalid content", value=message.content, inline=False)
        embed.add_field(name="Here is an example presentation for you", value=EXAMPLE_PRES, inline=False)
        await message.author.send(embed=embed)
