from .only_photos import OnlyPhotosPlugin
from .presentations import PresentationPlugin
from .valentin import ValentinPlugin
