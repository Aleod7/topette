from typing import List


class CommandError(Exception):
    ...


class CommandPermissionError(CommandError):
    def __init__(self, roles: List[str]):
        self.roles = roles

    def __str__(self):
        return f"You must have one these roles : {', '.join(self.roles)}"
