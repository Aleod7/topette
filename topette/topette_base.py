from __future__ import annotations
import asyncio
import abc
from functools import wraps
from typing import List, Collection, Callable, Any, Coroutine

from discord.ext import commands
from discord.ext.commands import Bot, Context
from discord.ext.commands.parameters import Signature
from sqlalchemy import create_engine
from sqlalchemy.orm import Session, declarative_base

from .exceptions import CommandPermissionError, CommandError

Base = declarative_base()


class TopetteBase(Bot):
    """Heart of the Topette bot, and actual discord.py client

    Intended to centralize and orchestrate all plugins
    """

    def __init__(self, *args, db_url: str, **kwargs):
        kwargs.setdefault('command_prefix', '/')
        super(TopetteBase, self).__init__(*args, **kwargs)
        self._plugins: List[TopettePlugin] = []
        self.engine = create_engine(db_url, echo=True, future=True)

    def register(self, plugin: TopettePlugin):
        """Add a new plugin to the TopetteBase plugin list"""
        self._plugins.append(plugin)
        plugin.on_registration(self)

    def __getattribute__(self, item: str):
        """When a handler is called, call the method it on every plugin implementing it"""

        def gather_all(methods: Collection[Callable[[Any], Coroutine]]):
            async def on_plugin(*args, **kwargs):
                return await asyncio.gather(*(method(*args, **kwargs) for method in methods))

            return on_plugin

        if item.startswith("on_") and (
            attr_in_plugins := [getattr(plugin, item) for plugin in self._plugins if hasattr(plugin, item)]
        ):
            try:
                main_method = super(TopetteBase, self).__getattribute__(item)
                if main_method:
                    attr_in_plugins.append(main_method)
            except AttributeError:
                ...
            return gather_all(attr_in_plugins)
        else:
            return super(TopetteBase, self).__getattribute__(item)

    async def on_ready(self):
        await self.tree.sync()
        Base.metadata.create_all(bind=self.engine)


class TopettePlugin(abc.ABC):
    """Abstract base for any Topette plugin

    Any inherited plugin must implement at least some asynchronous handlers, and be registered in the TopetteBase
    instance
    """

    _client: TopetteBase

    @property
    def session(self):
        return Session(self._client.engine)

    def on_registration(self, base: TopetteBase):
        """Method called as soon as the plugin is registered in the base

        Neither the base, nor the plugin will be ensured to have a discord connection yet, so it's better to
        overwrite `on_ready` if some discord API methods are needed for this plugin initiation
        """
        def handle_wrapper(name: str, method: Callable[[Any], Coroutine]):
            async def wrapper(ctx: Context, *args, **kwargs):
                try:
                    await method(ctx, *args, **kwargs)
                except CommandError as cderr:
                    await ctx.reply(f"Error : {cderr}", ephemeral=True)
                else:
                    await ctx.reply("Message registered", ephemeral=True)

            sig = Signature.from_callable(method)
            wrapper.__signature__ = sig

            return commands.hybrid_command(name=name)(wrapper)

        self._client = base
        for name, method in (
                (name, method)
                for name in dir(self)
                if name.startswith('_cmd_') and callable(method := getattr(self, name))
        ):
            self._client.add_command(handle_wrapper(name[5:], method))


def check_roles(role_names: List[str]):
    def wrap_function(func: Callable[[Any], Coroutine]):
        @wraps(func)
        async def wrapper(self, ctx: Context, *args, **kwargs):
            if not any(role.name in role_names for role in ctx.author.roles):
                raise CommandPermissionError(role_names)
            return await func(self, ctx, *args, **kwargs)
        return wrapper
    return wrap_function
