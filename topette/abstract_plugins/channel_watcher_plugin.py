import abc
import os
from typing import Collection, Optional

import discord
from discord.abc import GuildChannel

from topette.topette_base import TopettePlugin


class ChannelWatcherPlugin(TopettePlugin, abc.ABC):
    """Plugin to watch a channel, only allowing certain type of messages and deleting the others"""

    _channels: Collection[GuildChannel]

    def __init__(self, channel_ids: Optional[Collection[int]] = None):
        if not channel_ids:
            raw_channels = os.environ.get(f"{self.__class__.__name__.upper()}_CHANNEL_IDS")
            assert raw_channels, f"The plugin {self.__class__.__name__} needs some channels as arguments or " \
                                 f"environment variables."
            channel_ids = map(int, raw_channels.split(','))
        self._channel_ids = channel_ids

    async def on_ready(self):
        self._channels = [await self._client.fetch_channel(channel_id) for channel_id in self._channel_ids]
        await self._purge()

    async def _purge(self):
        for chan in self._channels:
            async for message in chan.history():
                if not await self._validate(message):
                    await self._on_invalid_message(message, history=True)
                    await message.delete()

    async def on_message(self, message: discord.Message):
        if message.channel in self._channels and not await self._validate(message):
            await self._on_invalid_message(message)
            await message.delete()

    @abc.abstractmethod
    async def _validate(self, message: discord.Message) -> bool:
        """Return a boolean to indicate whether the message is valid in the channel(s) or not"""
        raise NotImplementedError

    async def _on_invalid_message(self, message: discord.Message, history=False):
        """Can be overwritten to make an action when a message is not valid"""
        pass
