FROM python:3.11

COPY topette /topette
COPY main.py .
COPY requirements.txt .
RUN pip install -r requirements.txt
RUN rm requirements.txt


CMD python main.py